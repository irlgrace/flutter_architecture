import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'startup_viewmodel.dart';

class StartUpView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<StartUpViewModel>.reactive(
        builder: (context, model, child) => Scaffold(
          body: Center(
            child: Text('StartUp View'),
          ),
          floatingActionButton: FloatingActionButton(
              onPressed: () => model.navigateToHome(),
              child:Icon(
                Icons.arrow_back
              )
          ),
        ),
        viewModelBuilder: () => StartUpViewModel());
  }
}
